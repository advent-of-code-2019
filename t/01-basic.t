use v6.c;
use Test;
use AdventOfCode;

plan 8;

my $test-vm = IntcodeVM.new(slurp("./data/day2-a.txt"));

sub check-vm($input, $output, $name) {
    my $vm = IntcodeVM.new($input);
    $vm.run;
    is $vm.memory.join(','), $output, $name;
}

check-vm "1,0,0,0,99", "2,0,0,0,99", 'first example';
check-vm "2,3,0,3,99", "2,3,0,6,99", 'second example';
check-vm '2,4,4,5,99,0', '2,4,4,5,99,9801', 'third example';
check-vm '1,1,1,4,99,5,6,0,99', '30,1,1,4,2,5,6,0,99', 'last example';

{
    my $input = slurp("./data/day2-a.txt");
    my $vm = IntcodeVM.new($input);
    $vm.memory[1] = 12;
    $vm.memory[2] = 2;

    $vm.run();

    is $vm.output, 3760627, "day 2 first problem output";
}

subtest "VM don't change program after run", {
    my $input = '1,1,1,4,99,5,6,0,99';
    my $vm = IntcodeVM.new($input);
    $vm.run;
    is $vm.program, $input.split(','), 'simple input';
}

subtest "VM resets memory", {
    my $input = '1,1,1,4,99,5,6,0,99';
    my $vm = IntcodeVM.new($input);
    $vm.run;
    isnt $vm.memory, $vm.program, 'Run changes memory';
    my $memory = $vm.memory;
    $vm.reset-memory();
    is $vm.memory, $vm.program, '.reset-memory() resets memory to program';

    $test-vm.memory[1] = 12;
    $test-vm.memory[2] = 2;
    $test-vm.run;
    is $test-vm.output, 3760627, "day 2 first problem output";
    $test-vm.reset-memory;
    isnt $test-vm.output, 3760627, "day2 first problem resets memory";
}

subtest "day2 example", {
    my $input = slurp("./data/day2-a.txt");
    my $vm = IntcodeVM.new($input);
    $vm.run();
}


done-testing;
