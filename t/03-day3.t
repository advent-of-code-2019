use v6.d;
use Test;
use AdventOfCode::Utils;

#plan 2;

subtest "Point", {
    ok Point.new, 'Default Point creation';
    is Point.new.x, 0, 'Default x';
    is Point.new.y, 0, 'Default y';
    ok Point.new(:1x, :2y), 'Point creation';
}

subtest "Line", {
    my $point1 = Point.new;
    my $point2 = Point.new;

    ok Line.new($point1, $point2), 'without named parameters';
    ok Line.new(:$point1, :$point2), 'with named parameters';
}

subtest "Wire", {
    ok Wire.new("R1");
    my $wire = Wire.new("R1");

    is $wire.points.elems, 1, 'wire has one point';
    isa-ok $wire.points.head, Point, 'Elements are points';
    my $point = $wire.points.head;

    is $point.x, 1, "First wire point x {$point}";
    is $point.y, 0, 'First wire point y {$point}';

    subtest "intersection", {
        my $line1 = Line.new(
            :p(Point.new(:4x, :0y)),
            :q(Point.new(:6x, :10y))
        );
        my $line2 = Line.new(
            :p(Point.new(:0x, :3y)),
            :q(Point.new(:10x, :7y))
        );

        my $intersection = intersection($line1, $line2);

        is $intersection.x, 5, 'intersection x';
        is $intersection.y, 5, 'intersection y';
    }
}
done-testing;
