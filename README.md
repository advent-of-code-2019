NAME
====

AdventOfCode - blah blah blah

SYNOPSIS
========

```perl6
use AdventOfCode;
```

DESCRIPTION
===========

AdventOfCode is ...

AUTHOR
======

Matias Linares <matias.linares@comprandoengrupo.net>

COPYRIGHT AND LICENSE
=====================

Copyright 2019 Matias Linares

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

