use AdventOfCode::Utils;

sub minimum-distance($input1, $input2) {
    my $wire1 = Wire.new($input1);
    dd $wire1;
    my $wire2 = Wire.new($input2);

    for $wire1.lines -> $line1 {
        for $wire2.lines -> $line2 {
            if my $intersection = intersection($line1, $line2) {
                say "Manhattan distance of intersection $intersection: " ~ manhattan-distance(
                    Point.new, $intersection
                );
            }
        }
    }
}

sub MAIN('day3') is export(:main) {
    minimum-distance(
        "R8,U5,L5,D3",
        "U7,R6,D4,L4"
    );
}
